var {userModel} = require('../models/userModel');
exports.allTutorials =  (req, res) => {
    userModel.find().then(data=> {
        res.status(200).json({ message: "Product List", response: data })
    }).catch(error=> {
        res.status(404).json({ message: "No Item Found", response: error })
    });
}