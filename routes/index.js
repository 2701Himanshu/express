var express = require('express');
var router = express.Router();
var controller = require('../controllers/userController.js');
var auth=require('../modules/auth.js');

/* GET home page. */
router.route("/allTutorials").get(controller.allTutorials);

module.exports = router;
