const mongoose = require('mongoose');
const conn =  mongoose.createConnection('mongodb://localhost:27017/tuts', {useNewUrlParser: true});

conn.once('open', ()=>{console.log('Mongo DB Connected.')});
exports.mongoose =  mongoose;
exports.conn = conn;