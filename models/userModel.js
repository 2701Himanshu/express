const { mongoose, conn } =  require('../services/mongoose.js');
const Schema = mongoose.Schema;

let userSchema = new Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
    },
    password:{
        type: String,
    },
    age:{
        type: Number,
    },
    gender:{
        type: Number,
    },
    phone:{
        type: Number,
    },
    country:{
        type: String,
    }
});

exports.userModel = conn.model('user', userSchema);